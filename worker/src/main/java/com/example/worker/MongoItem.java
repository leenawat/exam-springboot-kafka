package com.example.worker;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document("items")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MongoItem {

    @Id
    private String id;

    private int msgId;
    private String sender;
    private String msg;
}
