package com.example.worker;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.item.Item;

@Service
public class KafkaConsumerService {

    @Autowired
    private ItemRepository itemRepository;

    @Value("${workerId:0}")
    private int workerId;

    @KafkaListener(topics = "items-topic", groupId = "sample-group", containerFactory = "kafkaListener")
    public void consume(Item item) {
        System.out.printf("# %d, %s++:%s, %s", workerId, item.getSender(), item.getMsg(), Instant.now());
        System.out.println("# " + workerId + ", " + item.getSender() + "++:" + item.getMsg() + ", " + Instant.now());
        MongoItem mongoItem = MongoItem.builder()
                .msgId(item.getMsgId())
                .msg(item.getMsg())
                .sender(item.getSender())
                .build();
        itemRepository.save(mongoItem);
    }
}