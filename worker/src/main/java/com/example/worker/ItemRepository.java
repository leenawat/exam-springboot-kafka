package com.example.worker;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface ItemRepository extends MongoRepository<MongoItem, String> {

}
