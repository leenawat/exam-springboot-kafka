package com.example.server;

import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.item.Item;
import com.example.item.ResponseItem;

@RestController
@RequestMapping("producer")
public class DemoController {

    @Autowired
    KafkaTemplate<String, Item> KafkaJsontemplate;
    String TOPIC_NAME = "items-topic";

    @PostMapping(value = "/item")
    public ResponseItem postJsonMessage(@RequestBody Item item) {
        System.out.println(item);
        Instant receivedTime = Instant.now();
        KafkaJsontemplate.send(TOPIC_NAME, item);
        return new ResponseItem("OK", receivedTime);
    }
}
