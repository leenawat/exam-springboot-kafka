package com.example.item;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Item {
    @JsonProperty("Msg_id")
    private int msgId;

    @JsonProperty("Sender")
    private String sender;

    @JsonProperty("Msg")
    private String msg;

}
