https://medium.com/blog-blog/spring-boot-apache-kafka-%E0%B9%81%E0%B8%9A%E0%B8%9A-basic-293fc311b578

```
version: '2'
services:
  zookeeper:
    image: wurstmeister/zookeeper
    ports:
      - "2181:2181"
  kafka:
    image: wurstmeister/kafka
    ports:
      - "9092:9092"
    environment:
      KAFKA_ADVERTISED_HOST_NAME: localhost
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
```
docker-compose up -d

application.properties
```
server.port=8010
spring.kafka.bootstrap-servers=localhost:9092
spring.kafka.consumer.group-id=myGroup
```

- https://www.onlinetutorialspoint.com/spring-boot/sending-spring-boot-kafka-json-message-to-kafka-topic.html

- https://www.onlinetutorialspoint.com/spring-boot/spring-boot-kafka-consume-json-messages-example.html

- https://github.com/eugenp/tutorials/blob/master/spring-kafka/src/main/java/com/baeldung/spring/kafka/KafkaApplication.java

- https://www.mongodb.com/compatibility/spring-boot

- https://www.baeldung.com/spring-scheduled-tasks

- https://www.baeldung.com/rest-template

- https://github.com/eugenp/tutorials/blob/master/spring-web-modules/spring-resttemplate-2/pom.xml

- https://www.baeldung.com/spring-resttemplate-post-json

- https://www.split.io/blog/containerization-spring-boot-docker/

- https://github.com/wurstmeister/kafka-docker/issues/575

- https://howtodoinjava.com/kafka/multiple-consumers-example/

- https://medium.com/@shrutishrm17/kafka-batch-processing-in-spring-boot-fc6c58f857fa

- https://reflectoring.io/spring-boot-kafka/

- https://github.com/TechPrimers/spring-boot-kafka-producer-example

- https://github.com/TechPrimers/spring-boot-kafka-consumer-example

- [Is it possible to add partitions to an existing topic in Kafka](https://stackoverflow.com/a/45101921/2462784)

- [Programmatically create Kafka topics using Spring Kafka](https://medium.com/@TimvanBaarsen/programmatically-create-kafka-topics-using-spring-kafka-8db5925ed2b1)

- https://github.com/wurstmeister/kafka-docker

- [kafka get partition count for a topic](https://stackoverflow.com/a/35438038/2462784)

- [When using @KafkaListener in springboot, how to set idleBetweenPolls](https://stackoverflow.com/a/62457295/2462784)

- [Chapter 4. Kafka Consumers: Reading Data from Kafka](https://www.oreilly.com/library/view/kafka-the-definitive/9781491936153/ch04.html)

