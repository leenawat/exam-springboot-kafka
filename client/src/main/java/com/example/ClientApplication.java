package com.example;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import com.example.item.ResponseItem;
@SpringBootApplication
public class ClientApplication {

    @Value("${serverUrl}")
    private String serverUrl;

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}

    @Scheduled(fixedDelay = 1000)
    public void scheduleFixedDelayTask() {
        int msgId = (int) (System.currentTimeMillis() / 1000);
        // System.out.println("Fixed delay task - " + msgId);
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Msg_id", msgId);
        jsonObject.put("Sender", "Tom");
        jsonObject.put("Msg", "Hello");

        HttpEntity<String> request = new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<ResponseItem> response = restTemplate
                .exchange(serverUrl + "/producer/item", HttpMethod.POST, request, ResponseItem.class);

        ResponseItem responseItem = response.getBody();
        System.out.println(responseItem);
    }

}
