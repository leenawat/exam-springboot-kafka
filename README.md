![](./messageImage_1646224264411.jpg)
```
cd client
mvn clean package -DskipTests
cd ..\server
mvn clean package -DskipTests
cd ..\worker
mvn clean package -DskipTests
docker-compose up-d
docker-compose logs -f
```

```
curl -X POST 'localhost:8082/producer/item' -H 'Content-Type: application/json' --data-raw '{
    "Msg_id":1,
    "Sender":"Tom...",
    "Msg":"Hello.."
}'
```

## เพิ่ม partition ให้ toppic เพื่อให้ consumer หลายตัวมาดึงจากแต่ละ partition
```
$ docker-compose exec kafka sh
$ /opt/kafka/bin/kafka-topics.sh --zookeeper zookeeper:2181 --topic items-topic --describe 
$ /opt/kafka/bin/kafka-topics.sh --zookeeper zookeeper:2181 --topic items-topic --alter --partitions 6
```
![](./2022-03-08_12h43_00.png)

***แก้แล้ว โดยการเพิ่ม partition***

ยังติดปัญหา Worker มาดึงข้อมูลจาก kafka ตัวเดียว ยังไม่สลับกันเข้ามาดึงข้อมูล

แต่หาตัวที่ทำงานอยู่ถูก stop ไป ตัวที่เหลือถึงจะมาดึงข้อมูลจาก kafka แทน